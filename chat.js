const requireconst = require("./require");
const User = require("./models/users");
const Rooms = require("./models/rooms");
const ObjectId = requireconst.mongoose.Types.ObjectId;
const indexref = require("./index"); 
var uid = '';
var otheruserdata= '';
var mysocketId = '';
var roomsData = [],userLiveData = [];
var sid = '',sdata='';
module.exports = function(app,io){
    var session = require("express-session")({
        secret: "my-secret",
        resave: true,
        saveUninitialized: true
    });
    var sharedsession = require("express-socket.io-session");

    // Attach session
    app.use(session);
    
    // Share session with io sockets
    
    io.use(sharedsession(session));

    io.on("connection",(socket)=>{
        console.log("user connected");
        socket.join("private_room");
        socket.on("login",(userId)=>{
            uid = userId;
            socket.handshake.session.userId = userId;
            socket.handshake.session.save();
            User.updateOne({"_id":ObjectId(userId.id)},{$set:{"socketId":socket.id,"status":"online"}},function(err,data){
                if(err){
                    console.log(err.message);
                } else if(data){
                   console.log("SocketId updated successfully ", socket.id);
                } 
            });
        });

        socket.on("checkOfflineOnline",(userId,roomId)=>{
            //console.log("roomid",roomId);
            Rooms.aggregate([{$match:{"users.userid":ObjectId(userId),"_id":ObjectId(roomId)}},{$lookup:{from:"users",localField:"users.userid",foreignField:"_id",as:"userdetail"}}],function(err,data){
                if(err){
                    console.log("Error in fetching rooms");
                } else{
                    console.log("Rooms detail fetched successfully!!!");
                    roomsData = JSON.parse(JSON.stringify(data));
                    roomsData.forEach(key=>{
                        key.userdetail.forEach(udata=>{
                            if(udata._id == userId){
                                console.log("udata",udata);
                                otheruserdata = udata.status;
                                sid = udata.socketId;
                            }
                            if(udata._id != userId){
                                mysocketId = udata.socketId; 
                                sdata = udata.status;
                            }
                        });
                    });
                    indexref.io.to(mysocketId).emit("OnlineOffline",otheruserdata);
                    indexref.io.to(sid).emit("OnlineOffline",sdata);
                }
            })
        });

        socket.on("typing",(recId)=>{
            User.findOne({_id:ObjectId(recId)},function(err,data){
                console.log(data);
                if(err){
                    console.log("error in fetching details");
                } else {
                    indexref.io.to(data.socketId).emit("isTyping","typing...");
                }
            });
        });

        socket.on("stoptype",(recId)=>{
            User.findOne({_id:ObjectId(recId)},function(err,data){
                console.log(data);
                if(err){
                    console.log("error in fetching details");
                } else {
                    indexref.io.to(data.socketId).emit("isTyping","");
                }
            });
        });

        socket.on("checkLiveStatus",(loginId)=>{
            userLiveData.length = 0;
            Rooms.aggregate([{$match:{"users.userid":ObjectId(loginId)}},{$lookup:{from:"users",localField:"users.userid",foreignField:"_id",as:"userdetail"}}],function(err,data){
                if(err){
                    console.log("Error in getting details");
                } else {
                    console.log("Room Users detail fetched successfully!!!");
                    roomsData = JSON.parse(JSON.stringify(data));
                    console.log("rdata ",roomsData);
                    roomsData.forEach(key=>{
                        key.userdetail.forEach(udata=>{
                            if(udata._id == loginId){
                                mysocketId = udata.socketId;
                            } else {
                                userLiveData.push(udata);
                            }
                            indexref.io.to(mysocketId).emit("userLiveStatus",userLiveData);
                        });
                    });
                }
            });
        });

        socket.on("logout", (userdata,roomId)=>{
            if (socket.handshake.session) {
                User.updateOne({"_id":ObjectId(userdata)},{$set:{"status":"offline"}},function(err,data){
                    if(err){
                        console.log(err.message);
                    } else if(data){
                       console.log("Status offline updated successfully ", socket.id);
                       console.log("roomid ",roomId);
                       Rooms.aggregate([{$match:{_id:ObjectId(roomId)}},{$lookup:{from:"users",localField:"users.userid",foreignField:"_id",as:"userdetail"}}],function(err,data){
                        if(err){
                            console.log("Error in fetching rooms");
                        } else{
                            console.log("Rooms detail fetched successfully!!!");
                            roomsData = JSON.parse(JSON.stringify(data));
                            console.log("rdata ",roomsData);
                            roomsData.forEach(key=>{
                                key.userdetail.forEach(udata=>{
                                    if(udata._id != userdata){
                                        mysocketId = udata.socketId;
                                    }
                                    indexref.io.to(mysocketId).emit("OnlineOffline","offline");
                                });
                            });
                        } 
                       });
                      
                    } 
                });
                delete socket.handshake.session.userId.id;
                socket.handshake.session.save();
            }
        });

        socket.on("ping",()=>{
            socket.emit("data");
        });

        socket.on("error",(param)=>{
            console.log("is is ", param);
        });

        socket.on("disconnect",(user)=>{
            console.log("user disconnect",uid);
            console.log("is is ", socket.id);
            User.updateOne({socketId : socket.id }, {$set : {"status" : "offline"}}, function(err, update){
                if(err){
                    console.log("Error in updating record!!!");
                }
                else{
                    console.log("Record Updated Successfully!!!");
                }
            })
        });
    });
}