
const bodyparser = require("body-parser");
const mongoose = require('mongoose');
const multer = require('multer');
const db = "mongodb://localhost:27017/livechat";
mongoose.connect(db,{useNewUrlParser:true});


module.exports.bodyparser = bodyparser;
module.exports.mongoose = mongoose;
module.exports.multer = multer;
