const requireconst = require("../require");

let Schema = requireconst.mongoose.Schema;

let MessageSchema = new Schema({
    roomId : Object,
    messages : String,
    senderId : Object,
    contentType: String,
    createdAt : String
});

module.exports = requireconst.mongoose.model("messages",MessageSchema);