const requireconst = require("../require");

let Schema = requireconst.mongoose.Schema;

let RoomsSchema = new Schema({
    isGroup : {type:Boolean,default:false},
    users : Array,
    groupPicUrl : {type:String,default:null},
    groupStatus : {type:String,default:null},
    groupName : {type:String,default:null},
    createdAt : {type:String,default:Date.now()}
});

module.exports = requireconst.mongoose.model("rooms",RoomsSchema);