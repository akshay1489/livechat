const requireconst = require("../require");

let Schema = requireconst.mongoose.Schema;
let UserSchema = new Schema({
    firstname : String,
    lastname : String,
    username : String,
    password : String,
    imageUrl : String,
    status : {type : String,default: "offline"},
    about : String,
    phone : String,
    socketId : String,
    token : String
});

module.exports = requireconst.mongoose.model("users",UserSchema);

