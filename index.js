const requireconst = require("./require");
const express = require("express");
const app = express();
const usersRouter = require('./controller/users');
const messagesRouter = require('./controller/messages');
const roomsRouter = require('./controller/rooms');
const chatsRouter = require('./controller/chats');
const chat = require('./chat');
//////////////////Socket IO connectivity////////////////////////
const http = require("http");
//const path = require('path');
const server = http.Server(app);
const socketIO = require("socket.io");
const io = socketIO(server);

app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
	res.header('Access-Control-Allow-Credentials', true);
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
	//res.append('Access-Control-Allow-Authentication',false);
	next();
});

io.attach(server, {
	pingTimeout: 60000,
});


chat(app,io);

app.use(express.static('public'));
//app.use(express.static(path.join(__dirname, "dist/angularlivechat")));
app.use(requireconst.bodyparser.json());
app.use(requireconst.bodyparser.urlencoded({extended:true}));
app.use('/',usersRouter);
app.use('/messages',messagesRouter);
app.use('/rooms',roomsRouter);
// app.use('/chats',chatsRouter);

/*app.get('*', function(req, res){
	res.sendFile(path.join(__dirname, '/dist/angularlivechat/index.html'))
})*/

server.listen('5050',()=>{
	console.log("Listening at port 5050");
});

module.exports.io = io;