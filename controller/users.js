const requireconst = require("../require");
const express = require('express');
const app = express();
const Users = require("../models/users");
const ObjectId = requireconst.ObjectId;

const config = {
    storage: requireconst.multer.diskStorage({
        destination: function(req,res,next){
            next(null,'./public/images/profilepic');
        },
        filename : function(req,file,cb){
            let ext = (file.originalname).substring(((file.originalname).length)-3);
            cb(null,Date.now() + '.' + ext);
        }
    })
}
const upload = requireconst.multer(config);
app.get('/getUsers/:key',(req,res)=>{
    let keyparam = req.params.key;
    Users.find({$or:[{firstname : {$regex:keyparam,$options:'m'}},{username:{$regex:keyparam,$options:'m'}}]},(err,data)=>{
        if(err){
            res.json({error:true,message:err.message});
        } else {
            if(data.length > 0){
                res.json({error:false,message:"All Users found",data:data});
            } else {
                res.json({error:true,message:"Something went wrong!!"});
            }
        }
    });
});

/****
    @getUserDetail/:loginId
******/

app.get('/getUserDetail/:loginId',(req,res)=>{
    Users.findOne({_id:req.params.loginId},(err,data)=>{
        if(err){
            res.json({error:true,message:err.message});
        } else {
            if(data){
                console.log("data: ",data);
                res.json({error:false,message:"User found",data:data});
            } else {
                res.json({error:true,message:"Something went wrong!!"});
            }
        }
    });
});

//upload profile pic
app.post("/uploadProfilePic",upload.single('profilepic'),(req,res,next)=>{
    if(req.file){
        res.json({error:false,message:"file Uploaded successfully",data:req.file});
    } else {
        res.json({error:true,message:"file Uploaded failed"});
    }
    res.end();
});

//login user
app.get('/login/:username/:password',(req,res)=>{
    Users.findOne({username:req.params.username,password:req.params.password},(err,data)=>{
        if(err){
            res.json({error:true,message:err.message});
        } else {
            if(data){ //login successfully
                res.json({error:false,message:"Data found",data:data});
            } else {
                res.json({error:true,message:"Something went wrong!!"});
            }
        }
    });
});

//register user
app.post("/register",upload.single("profilepic"),(req,res)=>{
    if(req.file){
        let firstname  = req.body.firstname;
        let lastname = req.body.lastname;
        let phone = req.body.phone;
        let imageUrl = req.file.filename;
        let about = req.body.about;
        let status = req.body.status;
        let username = req.body.username;
        let password = req.body.password;
        let token = (Math.random() * 9999999);
        console.log(username);
        if(username){
            let users = new Users({
                firstname:firstname,
                lastname:lastname,
                phone:phone,
                imageUrl : imageUrl,
                username:username,
                password:password,
                about:about,
                status:status,
                token:token
            });

            users.save((err,data)=>{
                if(err){
                    res.json({error:true,message:err.message});
                } else{
                    if(data){
                        res.json({error:false,message:"Record Saved Successfully!!",data:data});
                    } else {
                        res.json({error:true,message:"Error in saving record!!!"});
                    }
                }
            });
        } else {
            res.json({error:true,message:"Something went wrong!!!"});
        }
    } else {
        res.json({error:true,message:"File upload failed!!!"});
    }
});

module.exports = app;
