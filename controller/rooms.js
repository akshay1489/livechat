const requireconst = require("../require");
const express = require('express');
const app = express();
const Rooms = require('../models/rooms');
const ObjectId = requireconst.mongoose.Types.ObjectId;

//make rooms archive
app.put('/makeArchive',function(req,res,next){
    let roomId = req.body.roomId.split(",");

    for(let i=0 ; i<roomId.length;i++){
        Rooms.updateOne({"_id":ObjectId(roomId[i]),"users.userid":ObjectId(req.body.userId)},{$set:{"users.$.isArchive":Boolean(req.body.isArchive)}},(err,data)=>{
            if(err){
                res.json({error:true,message:"Something went wrong"});
                return false;
            } else {
                if(i == (roomId.length-1)){
                    next(null);
                }
            }
        });
    }
},function(req,res,cb){
    res.json({error:false,message:"Rooms Archived successfully!!!"});
});

//make admin
app.put('/makeAdmin',function(req,res,next){
    let userId = req.body.userId.split(",");
    for(let i=0;i<userId.length;i++){
        Rooms.updateOne({"_id":ObjectId(req.body.roomId),
        "users.userid":ObjectId(userId[i])},{$set:{"users.$.isAdmin":Boolean(req.body.isAdmin)}},(err,data)=>{
            if(err){
                res.json({error:true,message:err.message});
                return false;
            } else {
                if(i == (userId.length-1)){
                    next(null);
                }
            }
        });
    }
},function(req,res,next){
    res.json({error:false,message:"Admins role changed successfully!!!"});
});

//change status
app.put('/changeStatus',function(req,res){
    Rooms.updateOne({"_id":ObjectId(req.body.roomId),"users.userid":ObjectId(req.body.userId)},{$set:{"users.$.status":req.body.status}},(err,data)=>{
        if(err){
             res.json({error:true,message:err.message});
        } else {
            if(data){
                res.json({error:false,message:"Chats Archived Successfully!!!",data:data});
            } else {
               res.json({error:true,message:"Error in archiving chats"});
            }
        }
    });
});

//get archive rooms list
app.get('/archiveRooms/:userId',function(req,res,next){
    Rooms.find({"users.userid":ObjectId(req.params.userId),"users.isArchive":true},(err,data)=>{
        if(err){
            res.json({error:true,message:err.message});
        } else {
            if(data.length > 0){
                res.json({error:false,message:"Archive list fetched successfully!!!",data:data});
            } else {
                res.json({error:true,message:"Error in fetching archive list!!!"});
            }
        }
    });
});


//list rooms
app.get("/listrooms/:userid",(req,res,next)=>{
    Rooms.aggregate([{$match:{"users.userid":ObjectId(req.params.userid)}},{$lookup:{from:"users",localField:"users.userid",foreignField:"_id",as:"roomlist"}}],function(err,data){
        if(err){
            res.json({error:true,message:err.message});
        } else{
            if(data.length > 0){
                res.json({error:false,message:"Rooms list found",data:data});
            } else {
                res.json({error:true,message:"Rooms not found"});
            }
        }
    });
});

//get specific room
//list rooms
app.get("/getrooms/:roomId",(req,res,next)=>{
    Rooms.aggregate([{$match:{"_id":ObjectId(req.params.roomId)}},{$lookup:{from:"users",localField:"users.userid",foreignField:"_id",as:"roomlist"}}],function(err,data){
        if(err){
            res.json({error:true,message:err.message});
        } else{
            if(data.length > 0){
                res.json({error:false,message:"Rooms found",data:data});
            } else {
                res.json({error:true,message:"Rooms not found"});
            }
        }
    });
});

//save rooms
app.post('/saveRooms',(req,res)=>{
    console.log(req.body);
    let i = 0;
    let usrarr = [];
    let isGroup = req.body.isGroup;
    let users = req.body.users.split(',');
    let groupPicUrl = req.body.groupPicUrl;
    let groupStatus = req.body.groupStatus;
    let groupName = req.body.groupName;
    
    Rooms.findOne({$and:[{"users.userid":ObjectId(users[0])},{"users.userid":ObjectId(users[1])}]},function(err,data){
       
    if(!data) {
    for(let usr of users){
        usrarr.push({
            "userid" : ObjectId(usr),
            "isAdmin" : false,
            "status" : "offline",
            "isArchive" : false
        });
    }
    
    if(usrarr.length > 0){
        let rooms = new Rooms({
            isGroup : isGroup,
            users : usrarr,
            groupPicUrl : groupPicUrl,
            groupStatus : groupStatus,
            groupName : groupName
        });

        rooms.save((err,data)=>{
            if(err){
                res.json({error:true,message:err.message});
            }else{
                if(data){
                    res.json({error:false,message:"Rooms saved successfully!!",data:data});
                } else{
                    res.json({error:true,message:"Rooms not found!!"});
                }
            }
        });
    }
} else {
    if(err){
        res.json({error:true,message:err.message});
    }else{
        if(data){
            res.json({error:false,message:"Rooms saved successfully!!",data:data});
        } else{
            res.json({error:true,message:"Rooms not found!!"});
        }
    }
}
});

});

module.exports = app;