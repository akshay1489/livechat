const requireconst = require("../require");
const express = require('express');
const app = express();
const Messages = require("../models/messages");
const ObjectId = requireconst.mongoose.Types.ObjectId;
const Rooms = require("../models/rooms");
const User = require("../models/users");
const indexref = require("../index"); 

//get messages related to roomId
app.get('/getChats/:roomId',(req,res)=>{
    Messages.aggregate([{$match:{roomId:ObjectId(req.params.roomId)}},{$lookup:{from:"users",localField:"senderId",foreignField:"_id",as:"userDetail"}}],(err,data)=>{
        if(err){
            res.json({error:true,message:err.message});
        } else {
            if(data.length > 0){
                res.json({error:false,message:"Chats found",data:data});
            } else {
                res.json({error:true,message:"Something went wrong"}); 
            }
        }
    });
});

//get message
app.get('/getMessage/:messageId',(req,res)=>{
    Messages.aggregate([{$match:{_id:ObjectId(req.params.messageId)}},{$lookup:{from:"users",localField:"senderId",foreignField:"_id",as:"userDetail"}}],(err,data)=>{
        if(err){
            res.json({error:true,message:err.message});
        } else {
            if(data.length > 0){
                console.log("......>>>>>>",data);
                res.json({error:false,message:"Chats found",data:data});
            } else {
                res.json({error:true,message:"Something went wrong"}); 
            }
        }
    });
});

//save messages
app.post('/saveChats',(req,res)=>{
    let roomId = req.body.roomId;
    let messages = req.body.messages;
    let userId = req.body.senderId;
    let contentType = req.body.contentType;
    let createdAt = req.body.createdAt;

    if(roomId){
        let rid = ObjectId(roomId);
        let uid = ObjectId(userId);
        let msg = new Messages({
            roomId : rid,
            messages : messages,
            senderId : uid,
            contentType : contentType,
            createdAt : createdAt
        });

        msg.save((err,message)=>{
            flag = 0;
            if(err){
                res.json({error:true,message:err.message});
            }else{
                if(message){
                    Rooms.find({"_id":message.roomId},function(err,roomdata){
                        if(err){
                            flag = 0;
                        } else {
                           if(roomdata){
                           roomdata[0].users.forEach(key=>{
                                if(key.userid != userId){
                                    User.findOne({"_id":key.userid},function(err,data){
                                        if(err){
                                            console.log("Error in fetching user data");
                                        } else {
                                            console.log(data + ">>>>>>>>>>>>>>>>>>");
                                            indexref.io.to(data.socketId).emit("onMessageRecieved",message);
                                        }
                                    });
                                }
                            });
                           } else {

                           }
                            res.json({error:false,message:"User Details retrieved successfully",data:message});
                        }
                    });
                   // res.json({error:false,message:"Chat saved successfully!!",data:data});
                } else{
                    res.json({error:true,message:"Chats not found!!"});
                }
            }
        });
    }
});

app.put("/updateChats",(req,res)=>{
    let roomId = req.body.roomId;
    let messageId = req.body.messageId;
    let message = req.body.message;
    Messages.update({"roomId":ObjectId(roomId),"_id":messageId},{$set:{messages:message}},(err,data)=>{
        if(err){
            res.json({error:true,message:err.message});
        }else{
            if(data){
                res.json({error:false,message:"Chat updated successfully!!",data:data});
            } else{
                res.json({error:true,message:"Chat not found!!"});
            }
        }
    });
});

//delete chat
app.delete("/deleteChat/:chatId",(req,res)=>{
    Messages.deleteOne({_id:req.params.chatId},(err,data)=>{
        if(err){
            res.json({error:true,message:err.message})
        } else{
            if(data){
                res.json({error:false,message:"Chat deleted successfully!!",data:data});
            } else{
                res.json({error:true,message:"Chat not found!!"});
            }
        }
    });
});

//read chats
app.get("/readChats/:roomId",function(req,res){
    Messages.find({"roomId":ObjectId(req.params.roomId)},(err,data)=>{
        if(err){
            res.json({error:false,message:err.message});
        } else {
            if(data.length > 0){
                res.json({error:true,message:"Chats found successfully!!!",data:data});
            } else {
                res.json({error:false,message:"No chats found"});
            }
        }
    });
});

module.exports = app;
